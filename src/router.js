import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/faq',
      name: 'faq',
      component: () => import(/* webpackChunkName: "about" */ './views/FAQ.vue')
    },
    {
      path: '/porting',
      name: 'porting',
      component: () => import(/* webpackChunkName: "porting" */ './views/Porting.vue')
    }
  ]
})
