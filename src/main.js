import Vue from 'vue'
import VueI18n from 'vue-i18n'
import App from './App.vue'
import router from './router'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

Vue.use(VueI18n);

Vue.config.productionTip = false;

const i18n = new VueI18n({
    locale: 'en',
    messages: {
        en: require("./lang/en/en.js").messages
    }
});

new Vue({
    router,
    i18n,
    render: h => h(App)
}).$mount('#app');
