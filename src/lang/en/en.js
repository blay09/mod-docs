export const messages = {
    general: {
        backToOverview: "Back to Overview",
        crafting: "Crafting",
        usage: "Usage",
        related: "Related Items",
        versionNote: `
        Note that the information and recipes on this site are targetting the latest version of Minecraft. 
        Use a mod such as [Just Enough Items] if you need recipes from older versions.`,
        contribute: "Contribute to this wiki"
    },
    pagination: {
        previous: "Previous",
        next: "Next"
    },
    links: {
        curseforge: "[Downloads] on Curse",
        sourceCode: "[Source Code] on GitHub"
    },
    navbar: {
        overview: "Overview",
        blocks: "Blocks",
        items: "Items"
    },
    porting: {
        headline: "Porting Status",
        status: `Mostly waiting on things to stabilize. There's a bunch of things still missing in Forge, so I'm 
                waiting to see if they're getting implemented anytime soon before looking at workarounds.`,
        overallProgress: "Overall Progress",
        modsDone: "Mods updated: {0}",
        modsInProgress: "Mods in progress: {0}",
        modsPlanned: "Mods remaining: {0}",
        stats: "Stats",
        estimatedProgress: "Estimated Progress",
        modsDoneHeadline: "Beta Version available",
        modsInProgressHeadline: "In Progress",
        modsPlannedHeadline: "Not Started"
    },
    cookingforblockheads: require("./cookingforblockheads/cooking_for_blockheads.js").messages,
    faq: {
        headline: "Frequently Asked Questions",
        questions: require("./faq.js").messages
    }
};
