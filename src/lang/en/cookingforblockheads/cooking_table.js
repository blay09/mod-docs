export default {
    name: "Cooking Table",
    description: `
        The Cooking Table is the core of a multiblock kitchen. Right-clicking it will bring up the 
        interface of the [recipe book], in which you can quickly craft items using the ingredients in 
        your kitchen.
        
        In order for a kitchen to count as connected, blocks must be touching each other. The [Kitchen 
        Floor] can be used to connect blocks that are further apart.`,
    usage: `
        Place the cooking table down and connect it with other kitchen blocks, such as the fridge. Place 
        your ingredients into the fridge (or your inventory) and right-click the cooking table. On the 
        right you will see the foods you can craft with what you have available. Some of them may require 
        additional tools, as will be indicated in the recipe display on the left and the tooltip. Click on 
        a food once to see its recipe, click another time to craft one of it. In order to craft a stack, 
        shift-click instead.
        
        For recipes with multiple possible variations, you can click the arrow buttons that appear on the 
        left to navigate between them.
        
        For recipes with ore dictionary entries, the items in the recipe grid will flash up one after 
        another. In order to lock it to a specific item, you can click the slot in the recipe grid or use 
        your scroll wheel on it.
        
        The Cooking Table, like many kitchen blocks, can be dyed by right-clicking it with a dye.
        
        Right-clicking the cooking table with a #NoFilter edition will turn it into a #NoFilter Cooking 
        Table, making it so it always shows all recipes rather than just the ones you can cook right now.`
}
