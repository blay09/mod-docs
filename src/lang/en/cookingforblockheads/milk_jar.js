export default {
    name: "Toaster",
    description: `
        The Milk Jar is a small container able to hold eight buckets of milk. If used as part of a multiblock kitchen, 
        it will provide its milk to recipes that need it.

        Check the [Cooking Table] for more information on Multiblock Kitchens.`,
    usage: `
        Right-click with a bucket of milk to put milk into the milk jar. Right-click with an empty bucket to take it 
        back out.`
}
