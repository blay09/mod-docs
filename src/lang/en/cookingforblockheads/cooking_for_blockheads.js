export const messages = {
    name: "Cooking for Blockheads",
    tagline: "Adds a cooking book and multi-block kitchens that only shows recipes you can make with what you currently have in your inventory.",
    description: `
            Cooking for Blockheads allows you to build a functional kitchen in your Minecraft world and eases 
            the pain of complex cooking recipes. At its core is a recipe book that only shows you the foods 
            you can currently make with the ingredients you have available. For that it takes into account 
            your inventory, but also all connected kitchen blocks (such as fridges and cupboard), so you 
            can have all your food ingredients in one place. By expanding your kitchen with additional 
            blocks, you unlock new functionality, such as infinite water for recipes from the sink.
                
            It comes with support for a bunch of popular food mods, such as Pam's Harvestcraft.`,
    blocks: {
        cooking_table: import("./cooking_table"),
        oven: import("./oven"),
        fridge: import("./fridge"),
        tool_rack: import("./tool_rack"),
        spice_rack: import("./spice_rack"),
        counter: import("./counter"),
        corner: import("./corner"),
        kitchen_floor: import("./toaster"),
        milk_jar: import("./milk_jar"),
        cow_jar: import("./cow_jar"),
    },
    items: {
        recipe_book: import("./recipe_book")
    }
};
