export default {
    name: "Spice Rack",
    description: `
        The Spice Rack holds up to nine ingredients that will be made available to your multiblock kitchen.

        Check the [Cooking Table] for more information on Multiblock Kitchens.`,
    usage: `
        Right-click the spice rack with an item in your hand to put the item on the spice rack. Right-clicking the 
        spice rack when it already has an item in that spot will retrieve or swap the item out.

        You can also open the interface of the spice rack by right-clicking it with an empty hand and manage its 
        content that way.`
}
