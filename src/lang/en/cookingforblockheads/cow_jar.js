export default {
    name: "Toaster",
    description: `
        The Cow in a Jar is a genius invention based on the [Milk Jar].

        Inside is a tiny cow that will continuously produce milk over time.

        Check the [Cooking Table] for more information on Multiblock Kitchens.`,
    usage: `
        Right-click with a bucket of milk to put milk into the milk jar. Right-click with an empty bucket to take it 
        back out.`,
    instructionVideo: `
        If the above image is not clear enough, I've also made a short instructional video visualizing the process.`,
    craftingHint: "Drop an anvil block on a cow above a milk jar."
}
