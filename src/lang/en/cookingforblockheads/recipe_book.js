export default {
    name: "Recipe Book",
    description: `
        The Recipe Book is the main component of the Cooking Table, but it can also be used by itself. Right-clicking 
        it will bring up its interface, in which you can quickly view (and in the upgraded version, craft) items using 
        the ingredients you have in your inventory.`,
    usage: `
        Right-click the book to open its interface. On the right you will see the foods you can craft with what 
        ingredients you have available. Some of them may require additional tools, as will be indicated in the recipe 
        display on the left and the tooltip.

        Click on a food once to see its recipe. If you have Cooking for Blockheads 2 (Crafting Edition), click another 
        time to craft one of it. In order to craft a stack, shift-click instead.

        For recipes with multiple possible variations, you can click the arrow buttons that appear on the left to 
        navigate between them.

        For recipes with ore dictionary entries, the items in the recipe grid will flash up one after another. In order 
        to lock it to a specific item, you can click the slot in the recipe grid or use your scroll wheel on it.

        The #NoFilter Edition will show all available food recipes regardless of what ingredients you carry.`
}
