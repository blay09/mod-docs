export default {
    name: "Kitchen Counter",
    description: `
        The Kitchen Counter is a storage block for Multiblock Kitchens, similar to the [Fridge]. Ingredientss within 
        the counter will be made available for recipes in the Cooking Table.

        Check the [Cooking Table] for more information on Multiblock Kitchens.`,
    usage: `
        Right-click to open the kitchen counter's interface. It's basically like a chest.

        If you shift-right-click the door it will open the counter without opening the interface, allowing you to put 
        items in by simply right-clicking them into the front side.

        The Kitchen Counter, just like the Sink and Fridge, will be flipped horizontally based on the angle you place 
        it from.

        The Kitchen Counter, like many kitchen blocks, can be dyed by right-clicking it with a dye.`
}
