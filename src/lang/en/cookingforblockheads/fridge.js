export default {
    name: "Fridge",
    description: `
       The Fridge is a storage block for Multiblock Kitchens, similar to the [Kitchen Counter]. Ingredients within the 
       fridge will be made available for recipes in the Cooking Table.

        Check the [Cooking Table] for more information on Multiblock Kitchens.`,
    usage: `
        Right-click to open the fridge's interface. It's basically like a chest.

        If you shift-right-click the door it will open the fridge without opening the interface, allowing you to put 
        items in by simply right-clicking them into the front side.

        The Fridge, just like the Sink and Kitchen Counter, will be flipped horizontally based on the angle you place 
        it from.

        The Fridge, like many kitchen blocks, can be dyed by right-clicking it with a dye.`
}
