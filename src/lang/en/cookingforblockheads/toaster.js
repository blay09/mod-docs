export default {
    name: "Toaster",
    description: `
        The Toaster possesses magical heating powers to convert Untoasted Toast to Toast.

        It acts as a standalone block and does not provide any further functionality to a Multiblock Kitchen.

        Note that in order for the toaster to work, a mod adding Toast needs to be installed (e.g. Pam's Harvestcraft).`,
    usage: `
        Right-click the toaster with a supported type of bread (for Pam's, it's just normal Bread) to put it in. You 
        can put two pieces of bread into the toaster at once.

        Right-clicking the toaster again when it's full (or with an empty hand) will start the toasting process. After 
        a little time has passed, the finished toast will pop out at the top.`
}
