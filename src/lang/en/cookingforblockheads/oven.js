export default {
    name: "Oven",
    description: `
        The Oven can cook large amounts of food efficiently and fast. When used as part of a Multiblock 
        Kitchen, it will unlock smelting recipes in the Cooking Table.

        It also provides slots for four tools that will be available within the Multiblock Kitchen 
        (visually, they're for Pam's Harvestcraft tools, but you can place anything there).

        Check the [Cooking Table] for more information on Multiblock Kitchens.`,
    usage: `
        Place coal in the fuel slot on the left. Place uncooked food in the three slots at the top. The 
        oven will automatically pull nine items into its internal buffer, cook them, and them move them 
        over to the output slots on the right.

        If you shift-right-click the door it will open the oven without opening the interface, allowing 
        you to put items in by simply right-clicking them into the front side.`
}
