export default {
    name: "Tool Rack",
    description: `
        The Tool Rack holds up to two tools that will be made available to your multiblock kitchen. You can also just
        use it by itself as a ... well, tool rack.

        Check the [Cooking Table] for more information on Multiblock Kitchens.`,
    usage: `
        Right-click the tool rack with an item in your hand to put the item on the tool rack. Right-clicking the tool 
        rack when it already has an item in that spot will retrieve or swap the item out.`
}
