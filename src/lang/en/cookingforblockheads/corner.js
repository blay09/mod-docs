export default {
    name: "Kitchen Corner",
    description: `
        The kitchen corner can be used to connect kitchen blocks in corners where other blocks would look stupid.
        
        Check the [Cooking Table] for more information on Multiblock Kitchens.`,
    usage: `
        The Kitchen Corner, like many kitchen blocks, can be dyed by right-clicking it with a dye.`
}
