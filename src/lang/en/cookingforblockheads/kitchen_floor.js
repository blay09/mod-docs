export default {
    name: "Kitchen Floor",
    description: `
        The kitchen floor can be used to connect kitchen blocks to each other even when they're not directly touching.

        They are safe to use for decorational purposes as well.

        Check the [Cooking Table] for more information on Multiblock Kitchens.`,
    usage: ``
}
