export const messages = [
    {
        question: "Can I use your mod in a modpack?",
        answer: "All of my mods may be used in modpacks, please do not ask for express permission."
    },
    {
        question: "Can I repost your mod on a Chinese/Japanese/... etc. forum?",
        answer: `You are allowed to translate the mod description and repost it on your local forum. Please 
                    indicate that it's an not an official post and please do not ask for express permission.`
    },
    {
        question: "I have a great idea for a mod or new feature.",
        answer: `Please submit it on the appropriate GitHub issue tracker. If it's an idea for a mod, feel free to 
                    post it on our Discord server. Please do not send me mod ideas via DM.`
    },
    {
        question: "Can you backport to Minecraft 1.x.x please?",
        answer: `No, I only backport fixes for crashes and game-breaking bugs when necessary. New features will 
                    never be backported.`
    },
    {
        question: "Are you going to update your mod to Minecraft 1.x.x? When?",
        answer: `See the Porting Status link above. If there is none, come back in a week. If a mod is not listed 
                    there, a port is not currently planned. Mods will be ported as soon as possible.`
    },
    {
        question: "Thank you for answering all these questions, how do I give you money?",
        answer_html: `You can support me on <a href="https://www.patreon.com/blay09">Patreon</a>, subscribe on 
                        <a href="https://twitch.tv/BlayTheNinth">Twitch</a> or donate via 
                        <a href="https://streamlabs.com/blay09">Streamlabs</a>.`
    },
    {
        question: "Can I connect an Applied Energistics/Refined Storage network to my kitchen?",
        answer: "No, and it is very unlikely to ever happen."
    },
    {
        question: "Can I connect Storage Drawers to my kitchen?",
        answer: `In Minecraft 1.12+ yes, but due to technical reasons, you can only connect the actual drawers, not 
                    a drawer controller.`
    },
    {
        question: "Can you provide less modern looking models for the kitchen?",
        answer: `No, maintaining one set of models is already enough of a struggle. In newer versions, nearly 
                    everything should be replaceable through resource packs, and I'm happy to work with resource pack
                    creators in order to fix any remaining issues.`
    }
]
