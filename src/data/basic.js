export default {
    repository: "https://gitlab.com/blay09/mod-docs",
    social: {
        youtubeUrl: "https://www.youtube.com/channel/UC_oPeDy8En1rgTQqe0bzjEw",
        discordUrl: "https://discord.gg/scGAfXC",
        twitchUrl: "https://twitch.tv/BlayTheNinth",
        githubUrl: "https://github.com/blay09",
        patreonUrl: "https://patreon.com/blay09",
        twitterUrl: "https://twitter.com/BlayTheNinth"
    }
}
