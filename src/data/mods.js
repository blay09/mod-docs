export default {
    cookingforblockheads: require("./mods/cooking_for_blockheads.json"),
    farmingforblockheads: require("./mods/farming_for_blockheads.json"),
    refinedrelocation: require("./mods/refined_relocation.json"),
    clienttweaks: require("./mods/client_tweaks.json"),
    craftingtweaks: require("./mods/crafting_tweaks.json"),
    excompressum: require("./mods/ex_compressum.json"),
    fertilization: require("./mods/fertilization.json"),
    gravelminer: require("./mods/gravelminer.json"),
    hardcorerevival: require("./mods/hardcore_revival.json"),
    horsetweaks: require("./mods/horse_tweaks.json"),
    kleeslabs: require("./mods/kleeslabs.json"),
    prettybeaches: require("./mods/pretty_beaches.json"),
    trashslot: require("./mods/trashslot.json"),
    waystones: require("./mods/waystones.json"),
}
